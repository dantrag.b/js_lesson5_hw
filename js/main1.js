const currData = {
  "date": "22.03.2023",
  "bank": "PB",
  "baseCurrency": 980,
  "baseCurrencyLit": "UAH",
  "exchangeRate": [
    { "baseCurrency": "UAH", "currency": "AUD", "saleRateNB": 24.4370000, "purchaseRateNB": 24.4370000, "flag": "-640px 0px" },
    { "baseCurrency": "UAH", "currency": "AZN", "saleRateNB": 21.5490000, "purchaseRateNB": 21.5490000, "flag": "-800px 0px" },
    { "baseCurrency": "UAH", "currency": "BYN", "saleRateNB": 13.2919000, "purchaseRateNB": 13.2919000, "flag": "0px -40px" },
    { "baseCurrency": "UAH", "currency": "CAD", "saleRateNB": 26.7500000, "purchaseRateNB": 26.7500000, "flag": "-80px -80px" },
    { "baseCurrency": "UAH", "currency": "CHF", "saleRateNB": 39.5593000, "purchaseRateNB": 39.5593000, "saleRate": 41.6500000, "purchaseRate": 39.5500000, "flag": "-240px -440px" },
    { "baseCurrency": "UAH", "currency": "CNY", "saleRateNB": 5.3197000, "purchaseRateNB": 5.3197000, "flag": "-480px -80px" },
    { "baseCurrency": "UAH", "currency": "CZK", "saleRateNB": 1.6533000, "purchaseRateNB": 1.6533000, "saleRate": 1.7400000, "purchaseRate": 1.6500000, "flag": "-80px -120px" },
    { "baseCurrency": "UAH", "currency": "DKK", "saleRateNB": 5.2942000, "purchaseRateNB": 5.2942000, "flag": "-160px -120px" },
    { "baseCurrency": "UAH", "currency": "EUR", "saleRateNB": 39.4228000, "purchaseRateNB": 39.4228000, "saleRate": 41.1000000, "purchaseRate": 40.1000000, "flag": "0px -240px" },
    { "baseCurrency": "UAH", "currency": "GBP", "saleRateNB": 44.7892000, "purchaseRateNB": 44.7892000, "saleRate": 47.0300000, "purchaseRate": 44.6600000, "flag": "-320px -480px" },
    { "baseCurrency": "UAH", "currency": "HUF", "saleRateNB": 0.1007370, "purchaseRateNB": 0.1007370, "flag": "0px -200px" },
    { "baseCurrency": "UAH", "currency": "ILS", "saleRateNB": 10.0119000, "purchaseRateNB": 10.0119000, "flag": "-560px -200px" },
    { "baseCurrency": "UAH", "currency": "JPY", "saleRateNB": 0.2765600, "purchaseRateNB": 0.2765600, "flag": "-800px -200px" },
    { "baseCurrency": "UAH", "currency": "KZT", "saleRateNB": 0.0787490, "purchaseRateNB": 0.0787490, "flag": "-960px -200px" },
    { "baseCurrency": "UAH", "currency": "MDL", "saleRateNB": 1.9646000, "purchaseRateNB": 1.9646000, "flag": "-880px -280px" },
    { "baseCurrency": "UAH", "currency": "NOK", "saleRateNB": 3.4904000, "purchaseRateNB": 3.4904000, "flag": "-800px -320px" },
    { "baseCurrency": "UAH", "currency": "PLN", "saleRateNB": 8.3936000, "purchaseRateNB": 8.3936000, "saleRate": 8.8200000, "purchaseRate": 8.3800000, "flag": "-320px -360px" },
    { "baseCurrency": "UAH", "currency": "SEK", "saleRateNB": 3.5551000, "purchaseRateNB": 3.5551000, "flag": "-160px -440px" },
    { "baseCurrency": "UAH", "currency": "SGD", "saleRateNB": 27.3902000, "purchaseRateNB": 27.3902000, "flag": "-480px -400px" },
    { "baseCurrency": "UAH", "currency": "TMT", "saleRateNB": 10.4482000, "purchaseRateNB": 10.4482000, "flag": "-1120px -440px" },
    { "baseCurrency": "UAH", "currency": "TRY", "saleRateNB": 1.9219000, "purchaseRateNB": 1.9219000, "flag": "-1040px -440px" },
    { "baseCurrency": "UAH", "currency": "USD", "saleRateNB": 36.5686000, "purchaseRateNB": 36.5686000, "saleRate": 38.5000000, "purchaseRate": 38.0000000, "flag": "-400px -480px" }
  ]
}

const get_body = document.getElementById('body');

function showCurrency(currency_data) {
  const exchange_container = document.createElement('div');
  exchange_container.className = 'exchange_container';
  get_body.appendChild(exchange_container);

  function exhangeHeader(currency_data) {
    let curr_time = new Date();
    const exchange_header = document.createElement('div');
    exchange_header.className = 'exchange_header';
    exchange_container.appendChild(exchange_header);

    const exchange_date = document.createElement('div');
    exchange_date.className = 'exchange_date';
    exchange_header.appendChild(exchange_date);
    const exchange_date_span1 = document.createElement('span');
    const exchange_date_span2 = document.createElement('span');
    exchange_date_span1.classList.add("digits");
    exchange_date_span2.classList.add("text1");
    exchange_date.appendChild(exchange_date_span1);
    exchange_date.appendChild(exchange_date_span2);
    exchange_date_span1.appendChild(document.createTextNode(`${currency_data.date}`));
    exchange_date_span2.appendChild(document.createTextNode(`КУРСЫ ОБМЕНА ВАЛЮТ`));

    const exchange_time = document.createElement('div');
    exchange_time.className = 'exchange_time';
    exchange_header.appendChild(exchange_time);
    const exchange_time_span1 = document.createElement('span');
    const exchange_time_span2 = document.createElement('span');
    exchange_time_span1.classList.add("digits");
    exchange_time_span2.classList.add("text2");
    exchange_time.appendChild(exchange_time_span1);
    exchange_time.appendChild(exchange_time_span2);
    exchange_time_span1.appendChild(document.createTextNode(`${curr_time.getHours()}:${curr_time.getMinutes()}`));
    exchange_time_span2.appendChild(document.createTextNode(`EXCHANGE RATES`));
  }
  exhangeHeader(currency_data);


  function createTable(currency_data) {
    const tbl = document.createElement('table');
    exchange_container.appendChild(tbl);
    const tbl_head = document.createElement('thead');
    const tbl_body = document.createElement('tbody');
    tbl.appendChild(tbl_head);
    const head_tr = document.createElement('tr');
    tbl_head.appendChild(head_tr);
    for (let i = 0; i < 4; i++) {
      let head_td = document.createElement('td');
      if (i == 2) {
        head_td.appendChild(document.createTextNode(`ПОКУПКА`));
      }
      if (i == 3) {
        head_td.appendChild(document.createTextNode(`ПРОДАЖА`));
      }
      head_tr.appendChild(head_td);
    }

    tbl.appendChild(tbl_body);

    currency_data.exchangeRate.forEach(element => {
      const body_tr = document.createElement('tr');

      let body_td = document.createElement('td');
      body_td.classList.add("flag_pic");
      body_td.style.backgroundPosition = `${element.flag}`;
      body_tr.appendChild(body_td);

      let body_td1 = document.createElement('td');
      body_td1.appendChild(document.createTextNode(`${element.currency}`));
      body_td1.classList.add("curr_atr");
      body_tr.appendChild(body_td1);

      let body_td2 = document.createElement('td');
      body_td2.appendChild(document.createTextNode(`${(element.saleRateNB).toFixed(2)}`));
      body_td2.classList.add("digits");
      body_tr.appendChild(body_td2);

      let body_td3 = document.createElement('td');
      body_td3.appendChild(document.createTextNode(`${(element.purchaseRateNB).toFixed(2)}`));
      body_td3.classList.add("digits");
      body_tr.appendChild(body_td3);

      tbl_body.appendChild(body_tr);
    });
  }
  createTable(currency_data);
}

showCurrency(currData);